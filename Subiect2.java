import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class Subiect2 extends JFrame{
    JTextArea jTextArea;
    public Subiect2() {
        setTitle("Subiect 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(360, 360);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        jTextArea = new JTextArea("");
        jTextArea.setBounds(10, 10, 300, 200);
        jTextArea.setEditable(false);

        JButton button = new JButton("Buton");
        button.setBounds(10, 220, 300, 50);

        button.addActionListener(l -> {
            Random rand = new Random();
            int random = rand.nextInt(9999);
            jTextArea.append(random + "\n");
        });

        add(jTextArea);
        add(button);
    }

    public static void main(String[] args) {
       new Subiect2();
    }
}