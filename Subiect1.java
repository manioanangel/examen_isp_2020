
public class Subiect1 {
    public static void main(String[] args) {
        L l = new L();
        K k = new K(l);
        I i = new I(123, k);
    }
}

class I implements U, J{
    private long t;
    private K k;

    public I(long t, K k) {
        this.t = t;
        this.k = k;
    }

    public void t(){

    }

    public void i(J j) {
        
    }

    @Override
    public void p() {
        
    }
}

interface U {
    void p();
}

interface J {

}

class N {

}

class L {
    public void metA() {

    }
}

class S {
    public void metB() {

    }
}

class K {
    private S s = new S();
    private L l;

    public K(L l) {
        this.l = l;
    }
}